engine = require './engine'

'''
Implement chained triggers with promises
Look into writing a trigger/filter system
where trigger input/output can be intercepted
and transformed.
'''

/*
EXAMPLE

object.actions do
  describe: [triggers.message 'Hello world']
  mine: [
    triggers.message 'You mine the rock'
    triggers.delay 300
    triggers.add-item 'gold-ore'
  ]
*/

'''
Delay for x amount of time
'''
engine.triggers.create 'delay', (game) ->
  (deferred, delay) ->
    set-timeout deferred.resolve, delay
    deferred.promise

'''
Transport the game.player to another map
'''
engine.triggers.create 'transport', (game) ->
  (deferred, options) ->
    engine.maps.load options.name
    ...
    deferred.resolve!
    deferred.promise!

'''
Teleport the game.player to another location within
the map.
'''
engine.triggers.create 'teleport', (game) ->
  (deferred, options) ->
    game.player.position.x = options.x
    game.player.position.y = options.y
    deferred.resolve!

'''
Change a stat or attribute of a game.player
'''
engine.triggers.create 'modifyStat', (game) ->
  (deferred, options) ->
    game.player.stats[options.stat].increase options.amount
    deferred.resolve!

'''
Transform or exchange items from game.players inventory.
'''
engine.triggers.create 'transformItems', (game) ->
  (deferred, options) ->
    game.player.inventory.exchange options.required, options.exchange
    ...
    deferred.resolve!

'''
Remove item from game.players inventory
'''
engine.triggers.create 'removeItem', (game) ->
  (deferred, options) ->
    game.player.inventory.remove options.items
    ...
    deferred.resolve!

'''
Add an item to game.players inventory
'''
engine.triggers.create 'addItems', (game) ->
  (deferred, options) ->
    game.player.inventory.add options.items
    deferred.resolve!

'''
Random item from a list probability map
'''
engine.triggers.create 'randomItem', (game) ->
  (deferred, options) ->
    items = options.from
    random = null
    chance = Math.random!
    [random = item for item, rarity of items when rarity < chance]

    for item, rarity of items
      if rarity < chance
        random = item

    console.log "Random item! #random"
    engine.render.enqueue.object do
      img: engine.models.collections.items[random].img
      position:
        x: 6
        y: 5
    game.player.inventory.add random
    deferred.resolve!

'''
Issue a dialog
'''
engine.triggers.create 'showDialog', (game) ->
  (deferred, options) ->
    # show dialog with some options
    ...
    deferred.resolve!
'''
Write a message
'''
engine.triggers.create 'message', (game) ->
  (deferred, options) ->
    # write a message to the messagebox
    ...
    deferred.resolve!

'''
Create an object
'''
engine.triggers.create 'spawn', (game) ->
  (deferred, options) ->
    # place some object/entity within the world
    ...
    deferred.resolve!

'''
Create a trigger
'''
engine.triggers.create 'trigger', (game) ->
  (deferred, options) ->
    # Create a trigger somewhere
    ...
    deferred.resolve!

'''
Play some audio
'''
engine.triggers.create 'playAudio', (game) ->
  (deferred, options) ->
    # play some audio
    ...
    deferred.resolve!
