'''
Animation module

Internal clock, processing events only whilst
there are animations happening. RequestAnimationFrame
not supported on older versions of Android, so
a setTimeout hack or clearInterval will have to do.
'''

render = require '../render'
clock = require './clock'

animations = []

'''
Adds an animation onto the queue
and starts the animation clock
'''
!function add (animation)
  animations.push animation
  if clock.stopped
    clock.start process


'''
Increases the current frame and passes
the image to the renderer. Returns boolean
representing whether there are any more
frames left to display.
'''
function animate (animation)
  if animation.current-frame < animation.frame-count
    # extract this frame
    render.enqueue.fx animation
    render.flush.fx!
    animation.current-frame++
    return true
  else
    if not animation.infinite
      return false
    else
      animation.current-frame = 0
      return true

'''
If there are animations advance their frame
and if they are at the end of their reel,
remove them from the queue.
'''
!function process
  if animations.length > 0
    animations := animations.filter animate
  else
    clock.stop!

module.exports =
  add: add
  process: process
