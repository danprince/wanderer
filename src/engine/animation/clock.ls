'''
Clock Module

A clock which will run a given task as passed
in through clock.start and continue to run it at some
interval until clock.stop is called. It is possible 
to keep track of the clock's state using clock.stopped.
'''

fps = 5
interval = null
stopped = true

'''
Facade object which exposes methods and
properties (to prevent references from
being overwritten)
'''
clock =
  start: start
  stop: stop
  stopped: stopped

'''
Starts the clock running a job, providing 
the clock is not currently running one already
'''
!function start (job)
  if clock.stopped and interval is null
    interval := set-interval job, 1000 / fps
    clock.stopped = false

'''
Stops the job that is currently running and
clears the associated interval.
'''
!function stop
  clear-interval interval
  interval := null
  clock.stopped = true

module.exports = clock

