'''
Normalisation Module

Turns tile based coordinates, relative to the world
into real coordinates, relative to the camera.
'''

camera = require '../view/camera'
viewport = require '../view/viewport'
settings = require '../settings'

module.exports = (sx = 0, sy = 0, sw = 1, sh = 1, dx, dy, dw = 1, dh = 1) ->
  ss = settings.source-size
  ts = settings.tile-size

  # THIS IS WHAT IS BREAKING ANIMATION
  # origin within the image (default 0, 0)
  #sx *= ss
  #sy *= ss

  # size in world (assume ts * ts)
  dw = dw * ts
  dh = dh * ts
  # size of image in source
  sw *= ss
  sh *= ss
  # position of image within viewport
  dx = Math.floor ((dx - camera.position.x + viewport.size.width / 2) * ts)
  dy = Math.floor ((dy - camera.position.y + viewport.size.height / 2) * ts)
  return [sx, sy, sw, sh, dx, dy, dw, dh]
