visible = require './visible'
normalize = require './normalize'
queue = require './queue'
tiles = require '../tiles'

'''
Normalizes and queues an object, providing
that it is not culled by the bounds of
the current viewport.
'''
object = (object) !->
  {x, y} = object.position
  if visible x, y
    scale = object.scale or 1
    w = object.width or 1
    h = object.height or 1
    sx = 0
    sy = 0
    if object.offset
      sx = object.offset.x or 0
      sy = object.offset.y or 0
    [sx, sy, sw, sh, dx, dy, dw, dh] = normalize sx, sy, w, h, x, y, w * scale, h * scale
    queue.enqueue.object object.img, sx, sy, sw, sh, dx, dy, dw, dh

'''
Normalizes and queues an effect, providing
that it is not culled by the bounds of the
current viewport.
'''
fx = (animation) !->
  f = animation.current-frame
  {x, y} = animation.position
  if visible x, y
    w = animation.width
    h = animation.height
    [sx, sy, sw, sh, dx, dy, dw, dh] = normalize f, 0, w, h, x, y, w, h
    queue.enqueue.fx animation.img, sx, sy, sw, sh, dx, dy, dw, dh

'''
Normalizes and queues a tile for rendering,
providing it's not culled by the bounds of
the current viewport.
'''
tile = (tile, x, y, sides) !->
  if visible x, y
    [sx, sy, sw, sh, dx, dy, dw, dh] = normalize 0, 0, 1, 1, x, y, 1, 1
    queue.enqueue.tile tiles[tile].img, sx, sy, sw, sh, dx, dy, dw, dh, sides

'''

'''
light = (x, y, alpha) !->
  if visible x, y
    console.log x, y
    [sx, sy, sw, sh, dx, dy, dw, dh] = normalize 0, 0, 1, 1, x, y, 1, 1
    queue.enqueue.light alpha, dx, dy, dw, dh

module.exports =
  draw: require './draw'
  enqueue:
    object: object
    tile: tile
    fx: fx
    light: light
  flush: queue.flush
