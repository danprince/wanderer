'''
Drawing Module

Handles the creation of the rendering contexts
and the drawing of
'''

viewport = require '../view/viewport'
settings = require '../settings'

contexts =
  tile: null
  object: null
  fx: null
  light: null

image =
  tile: null
  object: null
  fx: null

'''
Curried function which takes a context and an image
and draws the image onto the context.
'''
draw-image = (ctx) ->
  (image, sx, sy, sw, sh, dx, dy, dw, dh, sides) ->
    ctx.draw-image image, sx, sy, sw, sh, dx, dy, dw, dh
    if sides
      # left
      if sides[0]
        ctx.begin-path!
        ctx.move-to dx, dy
        ctx.line-to dx, dy + dh
        ctx.stroke!
      # top
      if sides[1]
        ctx.begin-path!
        ctx.move-to dx, dy
        ctx.line-to dx + dw, dy
        ctx.stroke!
      # right
      if sides[2]
        ctx.begin-path!
        ctx.move-to dx + dw, dy
        ctx.line-to dx + dw, dy + dh
        ctx.stroke!
      # bottom
      if sides[3]
        ctx.begin-path!
        ctx.move-to dx + dw, dy + dh
        ctx.line-to dx, dy + dh
        ctx.stroke!

'''
Causes this canvas to expand to be the same size 
as the body of the page.
'''
fill-screen = (canvas, ctx, e) -->
  screen =
    x: window.inner-width
    y: window.inner-height
  canvas.width = screen.x
  canvas.height = screen.y
  ctx.webkitImageSmoothingEnabled = false
  ctx.mozImageSmoothingEnabled = false
  ctx.imageSmoothingEnabled = false

'''
Create a canvas element using name as a class name.
Designed to be used as an identifier for that class.
'''
create-canvas = (name) ->
  # create canvas and add name as class
  canvas = document.create-element 'canvas'
  $ canvas .add-class name

  # make context and create draw methods
  context = create-context canvas
  contexts[name] = context
  image[name] = draw-image context
  fill-screen canvas, context, null
  # update when canvas size changes
  $ window .on 'resize' fill-screen canvas, context
  canvas

'''
Extracts and returns a context from a canvas
'''
create-context = (canvas) ->
  context = canvas.get-context '2d'
  context.line-cap = 'round'
  context.line-width = 0
  context

'''
Inserts canvas into container
'''
insert-canvas = (container, canvas) -->
  container.append-child canvas
  canvas

'''
Creates a context and inserts the canvas element
for each context within the contexts variable
'''
create-contexts = (container) !->
  for name of contexts
    canvas = create-canvas name
    insert-canvas container, canvas

'''
Clears the canvas specified by name. 
'''
clear = (name) !->
  contexts[name].clear-rect 0, 0,
    viewport.size.width * settings.tile-size,
    viewport.size.height * settings.tile-size

'''
Draws a rectangle of light/dark onto the light canvas
'''
light = (alpha, x, y, w, h) !->
  contexts.light.fill-style = "rgba( 0, 0, 0, #alpha)"
  contexts.light.fill-rect x, y, w, h

'''
Renders a set of 3 rectangles to show the
3 canvases and the respective layering.
'''
debug = !->
  contexts.object.fill-style = 'red'
  contexts.object.fill-rect 0, 0, 100, 100
  contexts.tile.fill-style = 'blue'
  contexts.tile.fill-rect 80, 80, 100, 100
  contexts.fx.fill-style = 'green'
  contexts.fx.fill-rect 160, 160, 100, 100

module.exports =
  # Protected
  # expose so viewport can calulate bounds
  _contexts: contexts
  _debug: debug
  # Public
  clear: clear
  light: light
  image: image
  create-contexts: create-contexts
