'''
Visibility Module

Calculates whether coordinates are within the 
current viewport.
'''

camera = require '../view/camera'
viewport = require '../view/viewport'

module.exports = (x, y) ->
  width = Math.ceil viewport.size.width / 2
  height = Math.ceil viewport.size.height / 2
  (x >= camera.position.x - width) and
  (x < camera.position.x + width) and
  (y >= camera.position.y - height) and
  (y < camera.position.y + height)
