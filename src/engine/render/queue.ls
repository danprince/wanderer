'''
Image Queue

Queues up images for drawing within the different
canvases.
'''
draw = require './draw'

# One queue for each canvas
object-queue = []
tile-queue = []
fx-queue = []
light-queue = []

'''
Creates a function which pushes images
onto a queue, given as the initial argument.
'''
add = (queue) -> !->
  queue.push &

'''
Renders and empties every element within
a queue.
'''
flush = (name, queue) -> !->
  draw.clear name
  while image = queue.shift!
    draw.image[name].apply null, image

'''
Flush the light queue in a slightly
different way
'''
flush-light = !->
  console.warn 'Flush light'
  console.log light-queue
  while light = light-queue.shift!
    draw.light.apply null, light

module.exports =
  enqueue:
    object: add object-queue
    tile: add tile-queue
    fx: add fx-queue
    light: add light-queue
  flush:
    objects: flush 'object', object-queue
    tiles: flush 'tile', tile-queue
    fx: flush 'fx', fx-queue
    light: flush-light
