module.exports =

  # protected engine components
  render: require './render'

  # free to use
  progress: require './util/progress'
  camera: require './view/camera'
  viewport: require './view/viewport'
  input: require './input'
  save: require './save'
  tiles: require './tiles'
  animation: require './animation'
  # extracted properties
  models: require './models'
  map: require './maps' .map
  triggers: require './triggers'

  # core methods
  init: require './init'
  lighting: require './lighting'
