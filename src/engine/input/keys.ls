'''
Key Event Module

Listen to key events and register callbacks
for these events. Semantic name mapping for 
popular keyboard shortcuts.
'''

events = {}

map =
  \backspace : 8
  \tab : 9
  \enter : 13
  \ctrl : 17
  \alt : 18
  \caps : 20
  \esc : 27
  \left : 37
  \space : 32
  \up : 38
  \right : 39
  \down : 40
  \insert : 45
  \delete : 46

'''
Subscribe to an event with either a keycode
an event name or a keycode mapping from the
map above.
'''
subscribe = (name, fn) ->
  name = map[name] if map[name]?
  if not events[name]?
    events[name] = []
  events[name].push fn

'''
Trigger either a custom event by calling
it's name or passing an event object with
an associated keyCode.
'''
trigger = (event) ->
  name = | events[event]? => event
         | otherwise => event.key-code
  if events[name]?
    [fn! for fn in events[name]]

'''
Trigger key events every time a key is pressed
using the events keycode as a name.
'''
init = ->
  $ window .keydown trigger

'''
Initialise the listener when the document is ready
'''
$ document .ready init

module.exports =
  on: subscribe
  trigger: trigger
