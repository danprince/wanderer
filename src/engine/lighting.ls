# FOR 360 Degrees
# - Raycast from player to edge
# - - Gradually increment alpha
# - - If hit a surface, send alpha to 1

tiles = require './tiles'
{map} = require './maps'

module.exports = shader

'''
Raycasts around the map and calculates
alpha values for the light level at that
coorindate. Returns array of coordinates
with associated alpha values.
'''
function shader (player)
  {x, y} = player.position
  px = x
  py = y
  shaded = []
  dist = 10
  for r from 0 til 2 * Math.PI by 0.1
    x2 = px + ((Math.cos r) * dist)
    y2 = py + ((Math.sin r) * dist)
    points = raycast px, py, x2, y2
    alpha = 0
    for [x, y] in points
      shaded.push [x, y, alpha]
      alpha = | alpha < 1 => alpha + 0.1
              | tiles[map[x][y]].solid => 1
              | otherwise => 1
  shaded

'''
Calculates the equation of a line
and returns a function that will 
return the Y value for an X value
passed in.
'''
function line-equation  (x1, y1, x2, y2)
  m = (y2 - y1) / (x2 - x1)
  (x) -> m * (x - x1) + y1

'''
Casts a ray from P1 to P2 and returns
all points of ray in a list.
'''
function raycast (x1, y1, x2, y2)
  points = []
  line = line-equation ...
  step = | x2 < x1 => -1
         | otherwise => 1
  for x from x1 til x2
    y = Math.ceil line x
    points.push [x, y]
  points

