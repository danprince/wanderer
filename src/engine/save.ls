settings = require './settings'

'''
Loads a saved game from localstorage
'''
load = ->
  state = local-storage[settings.save-location]
  if state?
    try
      state = JSON.parse state
    catch message
      console.error 'Corrupted save file!'
      throw new Error message
  else
    console.warn 'No save found, starting a new game'
    state = {}
  state

'''
Saves a state object to localstorage
'''
save = (state) ->
  local-storage[settings.save-location] = JSON.stringify state

module.exports =
  load: load
  save: save
