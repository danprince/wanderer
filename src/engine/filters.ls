registered = {}

create = (name, filter) ->
  registered[name] = filter

module.exports =
  registered: registered
  create: create
