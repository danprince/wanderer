'''
Viewport Module

Singleton module for keeping track of the 
size of a viewport, to be used for culling
tiles.
'''

size =
  width: 0
  height: 0

'''
Updates the size of the of the viewport
'''
set-size = (width, height) ->
  size.width = width
  size.height = height

module.exports =
  size: size
  set-size: set-size
