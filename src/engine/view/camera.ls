'''
Camera Module

which can be bound to any entity with a position
property.
'''

position =
  x: 0
  y: 0

'''
Move the camera to a certain position
'''
move-to = (x, y) ->
  console.log x, y
  position.x = x
  position.y = y

'''
Attach the camera's position to be fixed
to the position of some entity. Designed
for focusing cameras on players.
'''
bind-to = (entity) ->
  position := entity.position

module.exports =
  position: position
  bind-to: bind-to
  move-to: move-to
