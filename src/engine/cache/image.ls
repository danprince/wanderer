'''
Image Cache

Used to cache images
'''

cache = require './cache'

module.exports = cache do
  build: (src) ->
    image = new Image
    image.src = src
    return [src, image]
