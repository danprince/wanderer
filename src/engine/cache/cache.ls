'''
Cache Module

Stores any data against a key for caching
purposes. Designed to be used with data
that will fire an onload event.
'''

progress = require '../util/progress'

data-cache = {}


module.exports = (model) ->

  events = []

  '''
  Creates an data item
  '''
  function create
    [id, data] = model.build ...
    progress.track-resource data
    cache id, data
    return data

  '''
  Gets data from the cache based on 
  an id. If it's not there, create it
  '''
  function get (id)
    # request cached object
    get-cached id or create ...

  '''
  Gets a cached version of some data
  '''
  function get-cached (id)
    data-cache[id]

  '''
  Caches an item of data
  '''
  function cache (id, data)
    data-cache[id] := data

  # debugging exposure that allows
  # us to see the caches
  get._debug = data-cache

  # User only needs to see the get
  # method, the rest goes on
  # behind the scenes.
  return get
