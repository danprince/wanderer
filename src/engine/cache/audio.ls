'''
Audio Cache

Used to cache audio
'''

cache = require './cache'

module.exports = cache do
  build: (src) ->
    audio = new Audio
    audio.src = src
    return [src, audio]
