'''
HTTP methods

Grabs some JSON and wraps it up 
in a promise, whilst ensuring
error throwing for failed requests.
'''
json = (url) ->
  deferred = Q.defer!
  $.getJSON url
  .then (data) ->
    deferred.resolve data
  .fail (error) ->
    deferred.reject error
  deferred.promise

module.exports =
  json: json
