'''
Resource loading progress module. 

Pass any resource
with an onload event as an argument to track-resource.
Register a promises with the loaded method. These
promises will be fulfilled when all the resources have
loaded.
'''

total = 0
tracking = 0
events = []

'''
Begins tracking a resource with a load
event.
'''
function track-resource (resource)
  tracking := tracking + 1
  total := total + 1
  #console.log "T#tracking"

  resource.onload = untrack-resource

'''
Stops tracking a resource and if finished
then calls the complete methods
'''
function untrack-resource
  tracking := tracking - 1
  #console.log "U#tracking"
  if tracking is 0
    complete!

'''
A decimal representing current progress of
resource loading.
'''
function progress
  (tracking / total) or 0

'''
Calls all the registered loaded events
to notify them that all the resources
have been loaded
'''
function complete
  for deferred in events
    deferred.resolve!

'''
Registers a promise for later resolution 
when the resource has been loaded
'''
function loaded
  deferred = Q.defer!
  events.push deferred
  deferred.promise

module.exports =
  track-resource: track-resource
  progress: progress
  loaded: loaded
