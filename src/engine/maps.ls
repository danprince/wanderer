http = require './util/http'

map =
  tiles: []
  objects: []

'''
Updates the singleton instance of map
with new map data.
'''
set = (new-map) ->
  map.tiles = new-map.tiles
  map.objects = new-map.objects

'''
Loads a map from the maps directory.
Maps should be of the format:
tiles: [2d array of integers]
objects: [array of objects]
'''
load = (name) ->
  deferred = Q.defer!
  http.json "maps/#name.json"
  .then (map) ->
    set map
    deferred.resolve map
  .fail (error) ->
    throw new Error "Could not load map: #name"
  deferred.promise


module.exports =
  map: map
  load: load
  set: set
