http = require './util/http'
progress = require './util/progress'
image-cache = require './cache/image'

'''
Collection of all models
'''
collections = {}

'''
Adds a model to the collection and indexes it
using its name as a key.
'''
add = (name, data) ->
  collections[name] = data

'''
Given a collection, create an instance of
the model found at name and add some options
features into it for initialisation.
'''
create = (model, name, options) -->
  if not collections[model]?
    throw new Error "No model exists with name #model"
  if not collections[model][name]?
    throw new Error "No model exists within #models with 
    the name #name"
  object = {}
  object <<< collections[model][name]
  object <<< options

'''
Loads a resource from the models directory.
Specify the name of the resource, assumed
to be JSON.
'''

load = (name) ->
  deferred = Q.defer!
  http.json "models/#name.json"
  .then (model) ->
    add name, model

    delete model['_schema']
    for key, object of model
      src = object.img
      object.img = image-cache "assets/#name/#src"

    deferred.resolve model
  .fail (error) ->
    console.error "Error loading #name models", error
    throw new Error error

module.exports =
  collections: collections
  load: load
  create: create
  add: add
