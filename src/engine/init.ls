# Initialise the engine
settings = require './settings'
models = require './models'
maps = require './maps'
draw = require './render/draw'
progress = require './util/progress'
viewport = require './view/viewport'
tiles = require './tiles'

'''
Events that need resolution before the game engine
can start.
'''
module.exports = ->
  deferred = Q.defer!

  Q.all [pre-dom!, progress.loaded!]
  .then !->
    ($ document) .ready ->
      post-dom!
      deferred.resolve!
  deferred.promise

'''
Actions that can be carried out before the DOM
has loaded.
'''
function pre-dom
  deferred = Q.defer!
  # Set up a resource progress
  Q.all [
    models.load 'items'
    models.load 'npc'
    models.load 'objects'
    models.load 'tiles'
    models.load 'characters'
    models.load 'fx'
    maps.load 'dungeon']
  .then (collection) !->

    # populate tile dictionary
    [tiles.push tile for name, tile of models.collections.tiles when name and name[0] is not '_']

    # load data from models into each object loaded in map 
    for object in maps.map.objects
      object <<< models.create 'objects', object.name, object

    # All collections have loaded
    deferred.resolve!
  .fail (errors) !->
    throw errors

  deferred.promise


'''
Interactions with the DOM once it has loaded
'''
function post-dom
  deferred = Q.defer!

  <-! $ document .ready
  do
    id = settings.canvas-container
    container = document.get-element-by-id id
    draw.create-contexts container

    # extract a canvas and use it to calculate
    # the size of the viewport
    canvas = draw._contexts.tile.canvas
    width = Math.ceil (canvas.width / settings.tile-size)
    height = Math.ceil (canvas.height / settings.tile-size)
    viewport.set-size width, height

    deferred.resolve!

  deferred.promise
