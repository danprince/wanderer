engine = require './'
#player = require './player'
#world = require './world'

registered = {}
filters = {}

create = (name, trigger) !->
  console.log "Create #name trigger"
  # trigger returns a function which
  # expects deferred and an options
  # object as arguments

  registered[name] = trigger

filter = (name, filter) ->
  filters[name] = filter

module.exports =
  registered: registered
  filters: filters
  filter: filter
  create: create
