size = 16
inventory = []
blocked = false

module.exports =
  add: add
  exchange: exchange
  remove: remove
  space: -> size - inventory.length

'''
Determines the amount of space within
the inventory.
'''
function space
  size - inventory.length

'''
Add items to inventory, will return
boolean showing whether they were
added or not.
'''
function add (items)
  if items not instanceof Array
    items = [items]
  if inventory.length < size
    inventory.push item
    return true
  else
    return false
    # show full message

'''
Remove item at index
'''
function remove (index)
  inventory.splice index, 1

'''
Determines whether these items are present
'''
function has-items (items)
  for 

'''
Concurrent method for exchanging
items 
'''
function exchange (taking, giving)
  taken = []
    for take in taking
    [taken.push remove index for index, item in inventory
        when take.id is item.id]
      if taken.length == taking.length
        if inventory.space! > giving.length
          inventory.add
