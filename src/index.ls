engine = require './engine'
triggers = require './triggers'

cursor =
  position:
    x: 0
    y: 0
  on: new Image
  off: new Image

cursor.on.src = 'assets/icons/cursor.png'
cursor.off.src = 'assets/icons/default-cursor.png'

player =
  position:
    x: 10
    y: 10
  character: 'hunter'
  weapon-drawn: false
  slots:
    # weapon
    "glassSword"
    # amulet
    "runeAmulet"
    # cape
    "shawl"
    # ring
    "blueRing"
    # shield
    "paintedRedShield"


collides = (x, y) ->
  if engine.tiles[engine.map.tiles[x][y]].solid
    return true
  else
    for object in engine.map.objects
      if object.solid and object.position.x == x and object.position.y == y
        return true

render = ->

  # add each tile and calculate it's sides
  tiles = engine.map.tiles
  for x from 0 til tiles.length
    for y from 0 til tiles[x].length
      tile = tiles[x][y]
      # work out which sides have a boundary on
      sides = [
        # left
        x - 1 > 0 and tile != tiles[x - 1][y]
        # up
        y - 1 > 0 and tile != tiles[x][y - 1]
        # right
        x + 1 < tiles.length and tile != tiles[x + 1][y]
        # down
        y + 1 < tiles[x].length and tile != tiles[x][y + 1]
      ]
      engine.render.enqueue.tile tiles[x][y], x, y, sides
  engine.render.flush.tiles!

  
  # render all npcs
  i = 0
  y = 0
  for id, npc of engine.models.collections.npc
    if i > 40
      i = 0
      y++
    engine.render.enqueue.object do
      img: npc.img
      width: npc.width
      height: npc.height
      position:
        x: i++
        y: y

  /*# render all npcs
  i = 0
  y = 4
  for id, item of engine.models.collections.items
    if id[0] is not '_'
      if i > 20
        i = 0
        y++
      engine.render.enqueue.object do
        img: item.img
        position:
          x: i++
          y: y

  # render all npcs
  i = 0
  for id, npc of engine.models.collections.objects
    if id[0] is not '_'
      engine.render.enqueue.object do
        img: npc.img
        width: npc.width
        height: npc.height
        position:
          x: i++
          y: 15
  */

  active = false
  active-object = null
  # add every object from the map
  # into the renderer
  objects = engine.map.objects
  for object in objects
    engine.render.enqueue.object object
    if object.position.x == cursor.position.x and object.position.y == cursor.position.y
      active = true
      active-object = object

  if active
    actions = active-object.actions
    # simulate choosing first action
    action = actions[Object.keys(actions)[0]]
    for let key, value of action
      game = { player }
      deferred = Q.defer!
      trigger = engine.triggers.registered[key]
      trigger = trigger game
      trigger deferred, value
      .then ->
        console.warn "FUCK YEAH #it"

    engine.render.enqueue.object do
      img: cursor.on
      position: cursor.position

  if player.slots[2]?
    # render cape
    engine.render.enqueue.object do
      img: player.slots[2].img
      position:
        x: player.position.x
        y: player.position.y

  # render player
  engine.render.enqueue.object player

  # render amulet
  if player.slots[1]?
    engine.render.enqueue.object do
      img: player.slots[1].img
      scale: 0.33
      offset:
        y: 8
      position:
        x: player.position.x + 0.35
        y: player.position.y + 0.4

  if player.weapon-drawn
    if player.slots[4]?
      # render shield
      engine.render.enqueue.object do
        img: player.slots[4].img
        scale: 0.66
        position:
          x: player.position.x + 0.35
          y: player.position.y + 0.3

    # render weapon
    if player.slots[0]?
      engine.render.enqueue.object do
        img: player.slots[0].img
        scale: 0.66
        position:
          x: player.position.x + 0.28
          y: player.position.y + 0.1

  engine.render.flush.objects!

update = !->
  if player.position.x - engine.viewport.size.width / 2 > 0
    if player.position.x + engine.viewport.size.width / 2 < engine.map.tiles.length
      engine.camera.position.x = player.position.x
    else
      engine.camera.position.x = engine.map.tiles.length - engine.viewport.size.width / 2
  else
    engine.camera.position.x = engine.viewport.size.width / 2

  # if viewport fits at TOP
  if player.position.y - engine.viewport.size.height / 2 > 0
    # if viewport fits at BOTTOM
    if player.position.y + engine.viewport.size.height / 2 < engine.map.tiles[0].length
      engine.camera.position.y = player.position.y
    else
     # viewport does not fit at BOTTOM
      engine.camera.position.y = engine.map.tiles[0].length - engine.viewport.size.height / 2
  else
    engine.camera.position.y = engine.viewport.size.height / 2
  # stop fx rendering in the wrong places
  engine.render.flush.fx!

init = !->
  engine.init!
  .then ->
    console.log engine.models
    # load player image
    player.img = engine.models.collections.characters[player.character].img

    # load triggers from map
    for key, object of engine.models.collections.objects
      if object.actions
        # for each action
        for name, action of object.actions
          # for each trigger
          console.log name

    # load each item from player slots
    for key of player.slots
      player.slots[key] = engine.models.collections.items[player.slots[key]]

    # add fire animation
    engine.animation.add engine.models.create 'fx', 'timer' do
      position:
        x: 8
        y: 13

    update!
    render!
  .fail -> console.error it

  # While resources are loading
  check-progress = ->
    loaded = engine.progress.progress!
    console.log "Loaded: #loaded%"
    if isNaN loaded or loaded < 1
      set-timeout check-progress, 100

  # start checking load progress
  check-progress!

  # controls
  engine.input.keys.on 'space' ->
    player.weapon-drawn = not player.weapon-drawn
  engine.input.keys.on 'left' ->
    if player.position.x - 1 >= 0
      cursor.position.x = player.position.x - 1
      cursor.position.y = player.position.y
      if not collides player.position.x - 1, player.position.y
        player.position.x--
        update!
      render!
  engine.input.keys.on 'right' ->
    if player.position.x + 1 <
    engine.map.tiles.length
      cursor.position.x = player.position.x + 1
      cursor.position.y = player.position.y
      if not collides player.position.x + 1, player.position.y
        player.position.x++
        update!
      render!
  engine.input.keys.on 'up' ->
    if player.position.y - 1 >= 0
      cursor.position.y = player.position.y - 1
      cursor.position.x = player.position.x
      if not collides player.position.x, player.position.y - 1
        player.position.y--
        update!
      render!
  engine.input.keys.on 'down' ->
    if player.position.y + 1 <
    engine.map.tiles[player.position.x].length
      cursor.position.y = player.position.y + 1
      cursor.position.x = player.position.x
      if not collides player.position.x, player.position.y + 1
        player.position.y++
        update!
      render!
  engine.input.keys.on 'ctrl' ->
    console.log 'Saving'
    engine.save.save do
      player:
        position: player.position
        character: player.character
window.onload = init
