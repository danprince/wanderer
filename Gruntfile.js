module.exports = function(grunt) {

  grunt.initConfig({
    
    'http-server': {
      dev: {
        root: './',
        port: 8282,
        host: 'localhost',
        runInBackground: true
      }
    },

    sass: {
      compile: {
        files: {
          './wanderer.css': 'styles/index.scss'
        },
        options: {
          debug: true
        }
      }
    },

    browserify: {
      compile: {
        files: {
          './wanderer.js': 'src/index.ls'
        },
        options: {
          transform: ['liveify'],
          extensions: ['.ls'],
          debug: true
        }
      }
    },

    uglify: {
      minify: {
        files: {
          './wanderer.min.js': './wanderer.js'
        }
      }
    },

    watch: {
      scripts: {
        files: ['src/**/*.ls'],
        tasks: ['browserify'],
        options: {
          livereload: true
        }
      },
      html: {
        files: ['index.html'],
        options: {
          livereload: true
        }
      },
      styles: {
        files: ['styles/**/*.scss'],
        tasks: ['sass'],
        options: {
          livereload: true
        }
      }
    }

  });

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-browserify');
  grunt.loadNpmTasks('grunt-http-server');

  grunt.registerTask('build', ['sass', 'browserify']);
  grunt.registerTask('minify', ['build', 'uglify']);
  grunt.registerTask('default', ['build', 'http-server', 'watch']);

};
