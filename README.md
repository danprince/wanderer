![Game](http://i.snag.gy/7fMuq.jpg)

#Todo

* Start Menu
* Picking a profession
* Dynamic lighting (raycasting + modifying image data with light kernel)
* (or like this) http://www.rockpapershotgun.com/2012/01/24/impressions-hack-slash-loot/
* Path Finding
* Design Pirate Ship tiles
* Design Red Dawn Altars + Temples
* Design city streets
* Design pub insides
* Design wilding huts
* Design goblin tents
* Design jackal pyramids
* Design dwarf buildings
* Design tree houses
* Design courtesanries
* Design bakeries
* Design cave features

## UI Screens
* Start menu
* Load Game
* Settings
* About
* Monologue
* Dialogue
* Skills/Abiltities
* Statistics
* Inventory/Equip

## Classes
* ![](assets/characters/wanderer.png) Wanderer -> ![](assets/characters/ranger.png) Ranger -> ![](assets/characters/freelance.png) Freelance  
* ![](assets/characters/apprentice.png) Apprentice -> ![](assets/characters/druid.png) Druid -> ![](assets/characters/warlock.png) Warlock  
* ![](assets/characters/archer.png) Archer -> ![](assets/characters/marksman.png) Marksman -> ![](assets/characters/hunter.png) Hunter  

## Skill Levels
| Class  | Strength | Precision | Magic | Ranged |
| ------ | -------- | --------- | ----- | ------ |
|Wanderer| 25       | 10        | 10    | 10     |
|Ranger  | 50       | 30        | 30    | 30     |
|Freelance| 150     | 100       | 100   | 100    |
|Apprentice| 10     | 10        | 25    | 10     |
|Druid   | 30       | 30        | 50    | 30     |
|Warlock | 100      | 100       | 150   | 100    |
|Archer  | 10       | 10        | 10    | 25     |
|Marksman| 30       | 30        | 30    | 50     |
|Hunter  | 100      | 100       | 100   | 150    |

## Skills
* ![](assets/icons/strength.png)__Strength__
* ![](assets/icons/precision.png)__Precision__
* ![](assets/items/fire-staff.png)__Magic__
* ![](assets/items/yew-bow.png)__Ranged__

===
Skills have a fixed number of levels. It is the level of skills
along with the class of player that determines what equipment
the player can wield. The base class will have a permanent
effect on the max skill for that type.

## Professions
| Icon | Profession  | Guild |
| ---  | ------------|-------|
| ![](assets/icons/conjurer.png) | __Conjurer__ | Ayantir |
| ![](assets/icons/sorcerer.png) | __Sorcerer__ | Ayantir |
| ![](assets/icons/enchanter.png) | __Enchanter__ | Ayantir |
| ![](assets/icons/craftsman.png) | __Craftsman__ | Drensen |
| ![](assets/icons/explorer.png) | __Explorer__ | N/A |
| ![](assets/icons/loremaster.png) | __Loremaster__ | Eldhall |
| ![](assets/icons/cleric.png) | __Cleric__ | Greenford |
| ![](assets/icons/pirate.png) | __Pirate__ | Gallow's Point |
| ![](assets/icons/smith.png) | __Smith__ | Threlkhold |
| ![](assets/icons/chef.png) | __Chef__ | Spicetown |
| ![](assets/icons/fisherman.png) | __Fisherman__ | Cedarwood |
| ![](assets/icons/thief.png) | __Thief__: Maezzir |
| ![](assets/icons/alchemist.png) | __Alchemist__ | Highwater City |
| ![](assets/items/cards.png) | __Gambler__ | Highwater City |

===
Professions are closed ended ways to make money and gain experience.
There are 7 statuses available for each profession. Ultimately, 
prowess in professions is what will define your player.

## Ranks
* Newcomer
* Neophyte
* Apprentice
* Journeyman
* Master
* Grandmaster
* Legendary

## Locations
### Cities
* Ayantir (Big island)
* Zaro'ch (Red Dawn Island)
* Thyst - Dwarf City
* Drensen - Dwarf City
* Frostmorn - Wildling Camp
* Highwater City - Main City
* Firport - North Port
* Cedarwood - Mid port
* Eldhall, Oldhearth, Greenford - River Towns
* Sequii Island - Djinn Lake
* Spicetown - South Port
* Maezzir - Pyramids

### Landmarks
* Temple at Zaro'ch
* Icetooth Range
* Sentine River + Source
* Icewaste
* Cartroad + Thousand Cedars
* Greyrock Hills
* Sandstown Ruins
* Pyramids at Maezzir
* Gallows Point (Pirates cave)
* Fountain District at Highwater
* Spice Mills at Spicetown
* Mill at Oldhearth
* Standing Stone at the center
* Thieves Den at Highwater
* Barrows of Ayantir
* Fighting Pits at Maezzir
* The Great Tomb
* Scorpion Caves

### Towns
__Men__
* Weatherstone (Druid village built around so called 'Weatherstone')
* Stonesteps (Village built into the mountain west of the river towns)
* Hillpass (Small fort built on a hill to protect traderoute)
* Coldrock (Northmost town of men, traderoute, prone to snow)
* Farmstowe (Farming capital at the delta of the Sentine)
* Sentineton (Bridge city at the crossing between Highwater and Dresen)
* Overtown (Guard town near southern border, used to be other side)
* Windy Top (Small village in southern foothills)
* Gablestead (Forest village in the Thousand Cedars)
* Nettow (Fishing village, west coast)
* Quillerton (Fishing village, west coast)

__Dwarves__
* Karmine (Underground port town, west of Thyst)
* Hoxdale (Underground town in the Greyrock Hills)
* Threlkhold (Northmost and deepest mine)

__Others__
* Ja'har (Semi permanent encampment north of Maezzir)
* Port Za'al (Port on Zaro'ch)
* Pentes (Island off of Zaro'ch)
* Si'quii (Island two in Djinnsea)

__Orcs and Goblins__
* Gragg's Camp (Moving encampment of orcs)
* Spoiler's Fort (Goblin & orc fort in Greyrock Hills)
* Dread's End (Orc & Pirate camp by Gallow's Point)
* Spitetown (Goblin town near central column)
* Myrkywater (Bog city)

##Craftsman

##![](assets/icons/smith.png)Smith
| Image | Ability     | Level |
| ----- | ----------- | ----- |
| ![](assets/items/hammer.png) | ![](assets/icons/padlock.png) Hammer | Newcomer |
| ![](assets/items/silver-rocks.png) | Mine Silver | Newcomer |
| ![](assets/items/silver-bar.png) | Smelt Silver| Newcomer |
| ![](assets/items/gold-rocks.png) | Mine Gold   | Neophyte |
| ![](assets/items/gold-bar.png) | Smelt Gold  | Neophyte |
| ![](assets/items/blue-rocks.png) | Mine Blue   | Apprentice |
| ![](assets/items/blue-bar.png) | Smelt Blue  | Apprentice |
| ![](assets/items/amethyst-hammer.png) | ![](assets/icons/padlock.png) Amethyst Hammer | Journeyman |
| ![](assets/items/green-rocks.png) | Mine Green  | Journeyman |
| ![](assets/items/green-bar.png) | Smelt Green | Journeyman |
| ![](assets/items/red-rocks.png) | Mine Red    | Master |
| ![](assets/items/red-bar.png) | Smelt Red   | Master |
| ![](assets/items/skyhammer.png) | ![](assets/icons/padlock.png) SkyHammer | Grandmaster |
| ![](assets/items/purple-rocks.png) | Mine Amethyst | Grandmaster |
| ![](assets/items/purple-bars.png) | Smelt Amethyst | Grandmaster |
| ![](assets/items/moon-rocks.png) | Mine Moonstone | Legendary |
| ![](assets/items/moon-bar.png) | Smelt Moonstone | Legendary |
| ![](assets/items/gilded.png) | Gild | Legendary

Lacatite
Malachite
Rubite

##![](assets/icons/chef.png)Chef
| Image | Ability     | Level |
| ----- | ----------- | ----- |
| ![](assets/items/ham.png) | Cook Ham | Newcomer |
| ![](assets/items/strawberry.png) | Pick Strawberry | Newcomer |
| ![](assets/items/fish.png) | Cook Fish   | Neophyte |
| ![](assets/items/bogfish.png) | Cook Bogfish   | Apprentice |
| ![](assets/items/icefish.png) | Cook Icefish  | Journeyman |
| ![](assets/items/crab.png) | Cook Crab  | Master |
| ![](assets/items/ale.png) | Brew Beer  | Master |
| ![](assets/items/rock-crab.png) | Cook Rockcrab | Grandmaster |
| ![](assets/items/nightfish.png) | Cook Nightfish | Legendary |

### Magic
Summon item / npc
Bolt/Burn/Blast

#Misc
* __Moonstone__ is only visible at night (unless you have moon googles).
* __Moonfish__ can only be caught at night (unless you have moon goggles).

#Tools

##Map Editor

* Quickly switch between maps
* Position tiles
* View triggers
* Place objects and attach triggers

##Item Editor

* List items
* View and edit properties
