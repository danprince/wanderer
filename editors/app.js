
/**
 * Module dependencies.
 */
var fs = require('fs');
var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use('/', express.static(path.join(__dirname, 'public')));
app.use('/', express.static(path.join(__dirname, '../')));
app.use('/assets', express.static(path.join(__dirname, '../assets')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

function api(type) {
  return function(data) {
    return {
      'status': type,
      'data': data
    }
  }
}

var error, success;
error = api('error');
success = api('success');

function load(folder) {
  return function(res, file) {
    var path = folder + '/' + file + '.json';
    fs.exists(path, function(exists) {
      if(exists) {
        fs.createReadStream(path).pipe(res);
      } else {
        res.json(error({
          name: 'File not found',
          message: 'Cannot GET ' + folder  + '/' + file
        }));
      }
    });
  }
}

function write(folder) {
  return function(res, file, data) { 
    var path = folder + '/' + file + '.json';
    data = JSON.stringify(data);
    fs.writeFile(path, data, function(err) {
      if(err) {
        res.json(error({
          name: 'Write failed',
          message: err
        }));
      } else {
        res.json(success({
          name: 'Write success',
          message: 'Written to file at ' + path
        }));
      }
    });
  }
}

var maps = {
  load: load('../maps'),
  write: write('../maps')
};

var models = {
  load: load('../models'),
  write: write('../models')
};

app.get('/maps/get/:name', function(req, res) {
  maps.load(res, req.params.name); 
});

app.post('/maps/write', function(req, res) {
  maps.write(res, req.body.name, req.body.data); 
});

app.get('/maps/edit', function(req, res) {
  res.render('maps');
});

app.get('/models/get/:name', function(req, res) {
  models.load(res, req.params.name);
});

app.post('/models/save', function(req, res) {
  models.write(res, req.body.name, req.body.data);
});

app.get('/models/edit', function(req, res) {
  res.render('models');
});

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
