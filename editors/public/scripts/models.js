angular.module('app', ['ngRoute'])
.config(function($routeProvider) {
  $routeProvider
  .when('/:collection', {
    templateUrl: '/templates/model.html',
    controller: 'view'
  })
  .otherwise({
    redirectTo: '/items'
  });
})
.filter(filters)
.factory(services)
.controller(controllers);

