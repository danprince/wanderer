var filters = {

  search: function() {
    return function(items, term) {
      var key, result = {};
      for(key in items) {
        if(!items[key].name) {
          items[key].name = '_DEFAULT_';
        }
        if(items[key].name.toLowerCase().indexOf(term) !== -1) {
          result[key] = items[key]; 
        }
      }
      return result;
    };
  },

  typeof: function() {
    return function(item) {
      return typeof item;
    }
  },

  isObject: function() {
    return function(item) {
      return typeof item === 'object';
    }
  }
};
