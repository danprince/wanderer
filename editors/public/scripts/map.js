angular.module('app', [])

.controller(controllers)
.filter(filters)
.factory(services)
.directive(directives);
