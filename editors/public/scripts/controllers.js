var controllers = {
  view: function($scope, $routeParams, models) {
    $scope.model = {};
    $scope.name = '';
    $scope.collection = [];
    $scope.search = '';
    $scope.activeIndex = 0;
    $scope.schema = {};
    $scope.collectionName = $routeParams.collection;

    $scope.load = function(name) {
      models.load(name)
      .then(function(res) {
        $scope.name = name;
        $scope.collection = res.data;
        $scope.schema = $scope.collection['_schema'];
      });
    };

    $scope.new = function() {
      var id = prompt('Model id');
      var url = [], name = [];

      for(var i = 0; i < id.length; i++) {
        // insert - after uppercase
        if(id[i] === id[i].toUpperCase()) {
          url.push('-');
          name.push(' ');
        }
        url.push(id[i].toLowerCase());
        name.push(id[i]);
      }

      name = name.join('');
      url = url.join('');
      name[0] = name[0].toUpperCase();
      $scope.collection[id] = {
        name: name,
        img: url + '.png'
      };
    };

    $scope.save = function() {
      models.save($scope.name, $scope.collection);
    };

    $scope.edit = function(id, index) {
      $scope.activeIndex = index;
      console.log(index);
      $scope.model = $scope.collection[id];
      return true;
    };

    $scope.load($scope.collectionName);
    // being naughty
    window.addEventListener('keydown', function(e) {
      var keys;
      if(e.keyCode === 38 || e.keyCode === 40) {
        keys = Object.keys($scope.collection);
        if(e.keyCode === 40) {
          $scope.activeIndex++;
          if($scope.activeIndex >= keys.length) {
            $scope.activeIndex = keys.length - 1; 
          }
        } else {
          $scope.activeIndex--;
          if($scope.activeIndex < 0) {
            $scope.activeIndex = 0;
          }
        }
        $scope.$apply();
      }
    });
  }
};
