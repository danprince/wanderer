var services = {
  models: function($http, $q) {
    
    function load(name) {
      var url, deferred;
      deferred = $q.defer();
      url = '/models/get/' + name;
      /* PROMISE */
      return $http.get(url)
    }

    function save(name, data) {
      var url, deferred;
      url = '/models/save';
      /* PROMISE */
      return $http.post(url, {
        name: name,
        data: data
      })
    }

    return {
      save: save,
      load: load
    }
  }
};
